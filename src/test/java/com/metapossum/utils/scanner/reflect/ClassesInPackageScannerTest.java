/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner.reflect;

import com.metapossum.utils.scanner.*;
import com.metapossum.utils.scanner.filters.*;
import com.metapossum.utils.scanner.filters.ChainedResourceFilter;

import java.io.*;
import java.lang.annotation.*;
import java.util.*;

import org.apache.commons.io.input.*;
import org.junit.*;

import static org.junit.Assert.*;

@ClassesInPackageScannerTest.TestAnnotation
public class ClassesInPackageScannerTest {

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface TestAnnotation {}

    @Test
    public void testFindsClasses() throws IOException {
        Set<Class<?>> classes = new ClassesInPackageScanner(
            ClassesInPackageScannerTest.class.getClassLoader(), false
        ).scan("com.metapossum");

        assertTrue(classes != null && classes.size() > 0);
        assertTrue(classes.contains(ClassesInPackageScannerTest.class));
    }

    @Test
    public void testFindSubclasses() throws IOException {

        Set<Class<? extends ResourceFilter>> classes = new ClassesInPackageScanner(
            ClassesInPackageScannerTest.class.getClassLoader(), false
        ).findSubclasses("com.metapossum", ResourceFilter.class);

        assertTrue(classes != null && classes.size() > 0);
        assertTrue(classes.contains(ResourceFilter.class));
        assertTrue(classes.contains(ChainedResourceFilter.class));
        assertTrue(classes.contains(ExtendsClassResourceFilter.class));

        Set<Class<? extends InputStream>> streamClasses = new ClassesInPackageScanner(
            ClassesInPackageScannerTest.class.getClassLoader(), false
        ).findSubclasses("org.apache.commons", InputStream.class);

        assertTrue(streamClasses != null && streamClasses.size() > 0);
        assertTrue(streamClasses.contains(TeeInputStream.class));
    }

    @Test
    public void testFindImplementers() throws IOException {

        Set<Class<? extends ResourceFilter>> classes = new ClassesInPackageScanner(
            ClassesInPackageScannerTest.class.getClassLoader(), false
        ).findImplementers("com.metapossum", ResourceFilter.class);

        assertTrue(classes != null && classes.size() > 0);
        assertFalse(classes.contains(ResourceFilter.class));
        assertTrue(classes.contains(ChainedResourceFilter.class));
        assertTrue(classes.contains(ExtendsClassResourceFilter.class));
    }

    @Test
    public void testFindAnnotatedClasses() throws Exception {

        Set<Class<?>> classes = new ClassesInPackageScanner(
            ClassesInPackageScannerTest.class.getClassLoader(), false
        ).findAnnotatedClasses("com.metapossum", TestAnnotation.class);

        assertTrue(classes != null && classes.size() == 1);
        assertTrue(classes.contains(getClass()));
    }
}
