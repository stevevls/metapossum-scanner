/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner.filters;

import com.metapossum.utils.scanner.ResourceFilter;

import org.junit.*;

import static org.junit.Assert.*;

public class ChainedResourceFilterTest {

    @Test
    public void testOr() {
       runFilterTest(ChainedResourceFilter.Mode.Or, true, false, true, true, true, false);
    }

    @Test
    public void testAnd() {
        runFilterTest(ChainedResourceFilter.Mode.And, true, false, false, false, true, false);
    }

    @Test
    public void testNot() {
        runFilterTest(ChainedResourceFilter.Mode.Not, false, true, false, false, false, true);
    }


    protected void runFilterTest(ChainedResourceFilter.Mode mode, boolean test1, boolean test2, boolean test3, boolean test4, boolean test5, boolean test6) {

        ChainedResourceFilter<String> filter = new ChainedResourceFilter<String>(
            mode,
            new ResourceFilter.AcceptEverythingResourceFilter<String>()
        );

        assertEquals(test1, filter.acceptScannedResource("foo"));

        filter = new ChainedResourceFilter<String>(
            mode,
            new ResourceFilter.AcceptNothingResourceFilter<String>()
        );

        assertEquals(test2, filter.acceptScannedResource("foo"));

        filter = new ChainedResourceFilter<String>(
            mode,
            new ResourceFilter.AcceptEverythingResourceFilter<String>(),
            new ResourceFilter.AcceptNothingResourceFilter<String>()
        );

        assertEquals(test3, filter.acceptScannedResource("foo"));

        filter = new ChainedResourceFilter<String>(
            mode,
            new ResourceFilter.AcceptNothingResourceFilter<String>(),
            new ResourceFilter.AcceptEverythingResourceFilter<String>()
        );

        assertEquals(test4, filter.acceptScannedResource("foo"));

        filter = new ChainedResourceFilter<String>(
            mode,
            new ResourceFilter.AcceptEverythingResourceFilter<String>(),
            new ResourceFilter.AcceptEverythingResourceFilter<String>(),
            new ResourceFilter.AcceptEverythingResourceFilter<String>()
        );

        assertEquals(test5, filter.acceptScannedResource("foo"));

        filter = new ChainedResourceFilter<String>(
            mode,
            new ResourceFilter.AcceptNothingResourceFilter<String>(),
            new ResourceFilter.AcceptNothingResourceFilter<String>(),
            new ResourceFilter.AcceptNothingResourceFilter<String>()
        );

        assertEquals(test6, filter.acceptScannedResource("foo"));
    }



    @Test
    public void testCantConstructWithNoFilters() {

        try {
            new ChainedResourceFilter<String>(null);
            fail("construct should fail with null filters");
        }
        catch(IllegalArgumentException e) {}

        try {
            //noinspection unchecked
            new ChainedResourceFilter<String>(new ResourceFilter[0]);
            fail("construct should fail with null filters");
        }
        catch(IllegalArgumentException e) {}
    }
}
