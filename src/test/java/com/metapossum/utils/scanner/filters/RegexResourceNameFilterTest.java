/*
 * Copyright 2012 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner.filters;

import org.junit.Test;

import com.metapossum.utils.scanner.ResourceNameFilter;

import static junit.framework.Assert.*;

public class RegexResourceNameFilterTest {

    @Test
    public void testFilter() {

        RegexResourceNameFilter filter = new RegexResourceNameFilter(".*Filter\\.class");

        assertTrue(filter.acceptResourceName("any.package", "Filter.class"));
        assertTrue(filter.acceptResourceName("any.package", "SomethingFilter.class"));
        assertFalse(filter.acceptResourceName("any.package", "Something.class"));

        assertTrue(filter.acceptResourceName("any.package", "SomethingFilter.class.foo"));
        filter.setCompleteMatch(true);
        assertFalse(filter.acceptResourceName("any.package", "SomethingFilter.class.foo"));
        assertTrue(filter.acceptResourceName("any.package", "SomethingFilter.class"));

        // still on complete match here...
        filter.setPackagePattern("any\\.package");
        assertTrue(filter.acceptResourceName("any.package", "SomethingFilter.class"));
        assertFalse(filter.acceptResourceName("any.package.other", "SomethingFilter.class"));

        filter.setCompleteMatch(false);
        assertTrue(filter.acceptResourceName("any.package.other", "SomethingFilter.class"));
    }

}
