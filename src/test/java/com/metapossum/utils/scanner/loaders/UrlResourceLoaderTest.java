/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner.loaders;

import com.metapossum.utils.scanner.*;

import java.io.*;
import java.net.*;
import java.util.*;

import org.apache.commons.io.*;
import org.junit.*;

import static junit.framework.Assert.*;

public class UrlResourceLoaderTest {

    @Test
    public void testLoaderWithFilesystem() throws Exception {
        runTest("com.metapossum.utils.scanner");
    }

    @Test
    public void testLoaderWithJar() throws Exception {
        runTest("org.apache.commons.lang");
    }


    void runTest(String packageName) throws IOException {

        UrlResourceLoader resourceLoader = new UrlResourceLoader();
        PackageScanner<URL> packageScanner = new PackageScanner<URL>(resourceLoader);

        Set<URL> urls = packageScanner.scan(packageName);
        assertNotNull("urls not null for package " + packageName, urls);
        assertTrue("found urls in package " + packageName, urls.size() > 0);

        // todo - some spot checking for members!

        // ensure that all of the URLs are valid by opening up the stream.
        for(URL url : urls) {
            InputStream input = null;
            try {
                input = url.openStream();
                assertNotNull("able to open input stream for url : " + url.toExternalForm(), input);
                assertTrue("stream has bytes for url : " + url.toExternalForm(), input.available() > 0);
            }
            finally {
                IOUtils.closeQuietly(input);
            }
        }
    }
}
