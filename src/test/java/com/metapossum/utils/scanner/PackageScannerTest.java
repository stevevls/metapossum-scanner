/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner;

import java.io.*;
import java.util.*;
import java.util.jar.*;

import org.apache.commons.lang.*;
import org.junit.*;

import static org.junit.Assert.*;

public class PackageScannerTest {

    static class StringResourceLoader implements ResourceLoader<String> {

        private boolean rejectJar = false;
        private boolean rejectFile = false;

        StringResourceLoader() {
            this(false, false);
        }

        StringResourceLoader(boolean rejectJar, boolean rejectFile) {
            this.rejectJar = rejectJar;
            this.rejectFile = rejectFile;
        }

        @Override
        public String loadFromJarfile(String packageName, JarFile jarFile, JarEntry entry) {

            if(rejectJar) {
                throw new IllegalStateException("shouldn't scan any jars for this test!");
            }

            return StringUtils.substringAfterLast(entry.getName(), "/");
        }

        @Override
        public String loadFromFilesystem(String packageName, File directory, String fileName) {

            if(rejectFile) {
                throw new IllegalStateException("shouldn't scan any directories for this test!");
            }

            return fileName;
        }
    }


    @Test
    public void testScanPackageWithJarEntries() throws IOException {

        PackageScanner<String> scanner = new PackageScanner<String>(StringUtils.class.getClassLoader(), new StringResourceLoader(false, true));

        scanner.setRecursive(false);
        Set<String> straight = scanner.scan(StringUtils.class.getPackage().getName());
        assertNotNull(straight);
        assertFalse(straight.size() == 0);
        assertTrue(straight.contains("StringUtils.class"));
        assertFalse(straight.contains("StopWatch.class"));

        scanner.setRecursive(true);
        Set<String> recursive = scanner.scan(StringUtils.class.getPackage().getName());
        assertNotNull(recursive);
        assertFalse(recursive.size() == 0);
        assertTrue(recursive.size() > straight.size());
        assertTrue(recursive.contains("StringUtils.class"));
        assertTrue(recursive.contains("StopWatch.class"));
    }

    @Test
    public void testScanPackageWithDirectories() throws IOException {

        PackageScanner<String> scanner = new PackageScanner<String>(getClass().getClassLoader(), new StringResourceLoader(true, false));

        scanner.setRecursive(false);
        Set<String> straight = scanner.scan("com.metapossum.utils.scanner");
        assertNotNull(straight);
        assertFalse(straight.size() == 0);
        assertTrue(straight.contains("PackageScannerTest.class"));
        assertFalse(straight.contains("ClassesInPackageScannerTest.class"));

        scanner.setRecursive(true);
        Set<String> recursive = scanner.scan("com.metapossum.utils");
        assertNotNull(recursive);
        assertFalse(recursive.size() == 0);
        assertTrue(recursive.size() > straight.size());
        assertTrue(recursive.contains("PackageScannerTest.class"));
        assertTrue(recursive.contains("ClassesInPackageScannerTest.class"));
    }


    @Test
    public void testWithClasspathFilter() throws IOException {

        PackageScanner<String> scanner = new PackageScanner<String>(StringUtils.class.getClassLoader(), new StringResourceLoader())
            .setClasspathFilter(new ClasspathFilter() {
                @Override
                public boolean acceptJarFile(JarFile jarFile) {
                    return new File(jarFile.getName()).getName().matches("commons-lang-(\\d+(\\.\\d+)*)\\.jar");
                }

                @Override
                public boolean acceptDirectory(File directory) {
                    return directory.getAbsolutePath().indexOf("test-classes") >= 0;
                }
            });

        Set<String> matches = scanner.scan("org.apache");
        assertTrue("scanner should have found matches", matches != null && matches.size() > 0);
        assertTrue("commons-lang should have been included by classpath filter", matches.contains("StringUtils.class"));
        assertFalse("commons-io should have been excluded by classpath filter", matches.contains("IOUtils.class"));

        matches = scanner.scan("com.metapossum");
        assertTrue("scanner should have found matches", matches != null && matches.size() > 0);
        assertFalse("non-test classes should have been excluded by directory filter", matches.contains("PackageScanner.class"));
        assertTrue("test classes should have been included by directory filter", matches.contains("PackageScannerTest.class"));
    }

    @Test
    public void testWithResourceNameFilter() throws IOException {

        PackageScanner<String> scanner = new PackageScanner<String>(StringUtils.class.getClassLoader(), new StringResourceLoader())
            .setResourceNameFilter(new ResourceNameFilter() {
                @Override
                public boolean acceptResourceName(String packageName, String fileName) {
                    return fileName.contains("ResourceFilter");
                }
            });

        Set<String> matches = scanner.scan("com.metapossum");
        assertTrue(matches != null && matches.size() > 0);
        assertTrue(matches.contains("ResourceFilter.class"));
        assertTrue(matches.contains("ChainedResourceFilter.class"));

        for(String match : matches) {
            assertTrue(match.contains("ResourceFilter"));
        }
    }

    @Test
    public void testWithResourceFilter() throws IOException {

        PackageScanner<String> scanner = new PackageScanner<String>(StringUtils.class.getClassLoader(), new StringResourceLoader())
            .setResourceFilter(new ResourceFilter<String>() {
                @Override
                public boolean acceptScannedResource(String item) {
                    return item.length() > 0 && item.charAt(0) == 'C';  // c is for cookie...
                }
            });

        Set<String> matches = scanner.scan("com.metapossum");
        assertTrue(matches != null && matches.size() > 0);
        assertTrue(matches.contains("ClasspathFilter.class"));
        assertTrue(matches.contains("ClasspathFilter.class"));

        for(String match : matches) {
            assertTrue(match.length() > 0 && match.startsWith("C"));
        }
    }
}
