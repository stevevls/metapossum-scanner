/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner.reflect;

import com.metapossum.utils.scanner.*;

import java.lang.reflect.*;

/**
 * {@link ResourceFilter} that accepts classes derived from a provided base class.
 *
 * <p>The base class can be either an interface or class...the acceptance test uses
 * {@link Class#isAssignableFrom(Class)}.</p>
 *
 * @see ResourceFilter
 * @see PackageScanner
 * @see ClassesInPackageScanner
 *
 *  @author Steve van Loben Sels
 */
public class ExtendsClassResourceFilter implements ResourceFilter<Class<?>> {

    protected Class baseClass;
    protected boolean concreteOnly;

    public ExtendsClassResourceFilter(Class baseClass, boolean concreteOnly) {
        this.baseClass = baseClass;
        this.concreteOnly = concreteOnly;
    }

    @Override
    public boolean acceptScannedResource(Class item) {
        return baseClass.isAssignableFrom(item) && (!concreteOnly || !(item.isInterface() || Modifier.isAbstract(item.getModifiers())));
    }
}
