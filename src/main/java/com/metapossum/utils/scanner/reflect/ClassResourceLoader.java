/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner.reflect;

import com.metapossum.utils.scanner.*;

import java.io.*;
import java.util.jar.*;

import org.apache.commons.lang.*;

/**
 * {@link ResourceLoader} for loading class files.
 *
 * <p>Can be configured to load inner classes or not.</p>
 *
 * @see ResourceLoader
 * @see PackageScanner
 * @see ClassesInPackageScanner
 *
 * @author Steve van Loben Sels
 */
public class ClassResourceLoader implements ResourceLoader<Class<?>> {


    protected ClassLoader classLoader;
    protected boolean includeInnerClasses;

    public ClassResourceLoader(ClassLoader classLoader, boolean includeInnerClasses) {
        this.classLoader = classLoader;
        this.includeInnerClasses = includeInnerClasses;
    }


    @Override
    public Class<?> loadFromJarfile(String packageName, JarFile jarFile, JarEntry entry) {
        return loadClassFromFile(packageName, StringUtils.substringAfterLast(entry.getName(), "/"));
    }

    @Override
    public Class<?> loadFromFilesystem(String packageName, File directory, String fileName) {
        return loadClassFromFile(packageName, fileName);
    }

    protected Class<?> loadClassFromFile(String packageName, String fileName) {
        if((fileName.endsWith(".class") && (includeInnerClasses || fileName.indexOf("$") < 0))) {
            try { return classLoader.loadClass(packageName + "." + StringUtils.substringBeforeLast(fileName, ".")); }
            catch(ClassNotFoundException e) {}
        }

        return null;
    }
}
