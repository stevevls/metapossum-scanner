/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner.reflect;

import com.metapossum.utils.scanner.*;

import java.lang.annotation.*;

/**
 * {@link ResourceFilter} that accepts classes a provided annotation.
 *
 * @see ResourceFilter
 * @see PackageScanner
 * @see ClassesInPackageScanner
 *
 * @author Steve van Loben Sels
 */
public class AnnotatedClassResourceFilter implements ResourceFilter<Class<?>> {

    protected Class<? extends Annotation> annoClass;

    public AnnotatedClassResourceFilter(Class<? extends Annotation> annoClass) {
        this.annoClass = annoClass;
    }

    @Override
    public boolean acceptScannedResource(Class<?> item) {
        return item.getAnnotation(annoClass) != null;
    }
}
