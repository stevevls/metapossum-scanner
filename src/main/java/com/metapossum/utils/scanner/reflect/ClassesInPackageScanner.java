/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner.reflect;

import com.metapossum.utils.scanner.*;
import com.metapossum.utils.scanner.filters.*;
import com.metapossum.utils.scanner.filters.ChainedResourceFilter;

import java.io.*;
import java.lang.annotation.*;
import java.util.*;

/**
 * A {@link PackageScanner} for finding and loading classes.
 *
 * <p>Provides a handful of convenience methods for solving some typical use cases:
 * <ul>
 *   <li>{@link #findSubclasses(String, Class)} - finds all classes or interfaces that extend from the class argument.
 *   <li>{@link #findImplementers(String, Class)} - finds concrete types that descend from the class argument.
 *   <li>{@link #findAnnotatedClasses(String, Class)} - finds all classes annotated with the class argument.
 * </ul>
 * </p>
 *
 * @author Steve van Loben Sels
 */
public class ClassesInPackageScanner extends PackageScanner<Class<?>> {


    @SuppressWarnings( {"UnusedDeclaration"})
    public ClassesInPackageScanner() {
        this(getDefaultClassLoader(), false);
    }

    public ClassesInPackageScanner(ClassLoader classLoader) {
        this(classLoader, false);
    }

    public ClassesInPackageScanner(ClassLoader classLoader, boolean includeInnerClasses) {
        super(new ClassResourceLoader(classLoader, includeInnerClasses));
    }

    public <T> Set<Class<? extends T>> findSubclasses(String packageName, Class<T> baseClass) throws IOException {
        // javac fails to compile without the generic clue
        // noinspection RedundantTypeArguments
        return this.<T>scanWithExtraFilter(packageName, new ExtendsClassResourceFilter(baseClass, false));
    }

    public <T> Set<Class<? extends T>> findImplementers(String packageName, Class<T> baseClass) throws IOException {
        // javac fails to compile without the generic clue
        //noinspection RedundantTypeArguments
        return this.<T>scanWithExtraFilter(packageName, new ExtendsClassResourceFilter(baseClass, true));
    }

    public Set<Class<?>> findAnnotatedClasses(String packageName, Class<? extends Annotation> annoClass) throws IOException {
        return scanWithExtraFilter(packageName, new AnnotatedClassResourceFilter(annoClass));
    }

    protected <T> Set<Class<? extends T>> scanWithExtraFilter(String packageName, ResourceFilter<Class<?>> extraFilter) throws IOException {

        ResourceFilter<Class<?>> currentFilter = this.resourceFilter;
        try {
            this.resourceFilter = new ChainedResourceFilter<Class<?>>(
                ChainedResourceFilter.Mode.And,
                extraFilter,
                currentFilter
            );

            Set classes = scan(packageName);

            //noinspection unchecked
            return (Set<Class<? extends T>>) classes;
        }
        finally {
            this.resourceFilter = currentFilter;
        }
    }
}
