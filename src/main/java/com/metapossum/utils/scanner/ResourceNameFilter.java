/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner;

/**
 * Filter for resource by name just before they are loaded.
 *
 * @see PackageScanner
 *
 * @author Steve van Loben Sels
 */
public interface ResourceNameFilter {

    /**
     * Convenience implementation that doesn't filter anything.
     */
    public class AcceptEverythingResourceNameFilter implements ResourceNameFilter {

        @Override
        public boolean acceptResourceName(String packageName, String fileName) {
            return true;
        }
    }


    public boolean acceptResourceName(String packageName, String fileName);
}
