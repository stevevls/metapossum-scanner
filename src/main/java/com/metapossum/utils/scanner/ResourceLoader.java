/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner;

import java.io.*;
import java.util.jar.*;

/**
 * Player responsible for loading a resource from the classpath.
 *
 * <p>By using an interface, the {@link PackageScanner} can be used to discover different
 * types of resources.</p>
 *
 * <p>The most typical use will be to load classes with {@link com.metapossum.utils.scanner.reflect.ClassResourceLoader}.</p>
 *
 * @see PackageScanner
 * @see com.metapossum.utils.scanner.reflect.ClassResourceLoader
 *
 * @author Steve van Loben Sels
 */
public interface ResourceLoader<T> {

    public T loadFromJarfile(String packageName, JarFile jarFile, JarEntry entry);

    public T loadFromFilesystem(String packageName, File directory, String fileName);
}
