/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner;

/**
 * Callback for the {@link PackageScanner} from the {@link PackageScanner#visit(ResourceVisitor, String)} method.
 *
 * <p>The {@link PackageScanner} will call {@link #visit(Object)} once for each resource that it has loaded
 * and that passes all filter criteria.</p>
 *
 * @see PackageScanner
 * @see ResourceLoader
 *
 * @author Steve van Loben Sels
 */
public interface ResourceVisitor<T> {

    /**
     * called once for each resources that has been loaded and passed all filter criteria.
     * @param resource as loaded by a {@link ResourceLoader} implementation.
     */
    public void visit(T resource);
}
