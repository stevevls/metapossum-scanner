/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.jar.*;
import java.util.regex.*;

import org.apache.commons.io.*;
import org.apache.commons.lang.*;

/**
 * Utility for discovering resources in a given package.
 *
 * <p>The scanner will search under all relevant classpath entries, both jars and
 * filesystem directories.  There are two modes of operation : scanning or visiting.
 * In the former, a set of resources is returned.  In the latter, a {@link ResourceVisitor}
 * will receive a callback for each found resource.</p>
 *
 * <p>An instance of the scanner is generic so that any resource can be loaded.  The
 * constructor requires an instance of an {@link ResourceLoader} so that it knows how to
 * treat the resources.  The most common use case will be to load classes, for which you
 * can either directly use the {@link com.metapossum.utils.scanner.reflect.ClassesInPackageScanner}
 * or instantiate this class with a {@link com.metapossum.utils.scanner.reflect.ClassResourceLoader}.</p>
 *
 * <p>There are a handful of ways to customize the search path.  Scans/visits can be
 * recursive or not, plus implementations of the following can be provided:
 * <ul>
 *   <li>{@link ClasspathFilter} - select which classpath entries are traversed.</li>
 *   <li>{@link ResourceNameFilter} - specify whether a resource is loaded based on its name.</li>
 *   <li>{@link ResourceFilter} - specify on an individual basis whether to accept a resource or not. (<b>NOTE:</b> resource has already been loaded)</li>
 * </ul>
 * </p>
 *
 * <p>Filters will be applied in the order given above.  In the case where loading an resource is expensive
 * (such as loading a class from a file), the first two filters can be used to optimize scanning because
 * they are applied before the resource is loaded.</p>
 *
 * <p>By default, instances of the scanner are created with no filters.</p>
 *
 * @author Steve van Loben Sels
 * @param <T> the resource type
 */
public class PackageScanner<T> {


    protected ClassLoader classLoader;
    protected ResourceLoader<T> resourceLoader;
    protected boolean recursive = true;

    protected ClasspathFilter classpathFilter = new ClasspathFilter.AcceptEverythingClasspathFilter();
    protected ResourceNameFilter resourceNameFilter = new ResourceNameFilter.AcceptEverythingResourceNameFilter();
    protected ResourceFilter<T> resourceFilter = new ResourceFilter.AcceptEverythingResourceFilter<T>();

    /**
     * Creates an instance with the {@link ResourceLoader} and a default {@link ClassLoader}.
     * @param resourceLoader
     */
    public PackageScanner(ResourceLoader<T> resourceLoader) {
        this(getDefaultClassLoader(), resourceLoader);
    }

    /**
     * Advanced : use this constructor if you need to specify the {@link ClassLoader}.
     * @param classLoader
     * @param resourceLoader
     */
    public PackageScanner(ClassLoader classLoader, ResourceLoader<T> resourceLoader) {
        this.classLoader = classLoader;
        this.resourceLoader = resourceLoader;
    }

    
    public PackageScanner<T> setRecursive(boolean recursive) {
        this.recursive = recursive;
        return this;
    }

    public PackageScanner<T> setClasspathFilter(ClasspathFilter classpathFilter) {
        this.classpathFilter = classpathFilter;
        return this;
    }

    public PackageScanner<T> setResourceNameFilter(ResourceNameFilter resourceNameFilter) {
        this.resourceNameFilter = resourceNameFilter;
        return this;
    }

    public PackageScanner<T> setResourceFilter(ResourceFilter<T> resourceFilter) {
        this.resourceFilter = resourceFilter;
        return this;
    }


    /**
     * performs an exhaustive recursive scan under package <code>packageName</code>.
     * @param packageName
     * @return
     * @throws IOException
     */
    public Set<T> scan(String packageName) throws IOException {
        final Set<T> result = new HashSet<T>();
        visit(new ResourceVisitor<T>() {
            @Override
            public void visit(T resource) {
                result.add(resource);
            }
        }, packageName);

        return result;
    }

    public void visit(ResourceVisitor<T> visitor, String packageName) throws IOException {

        // on windows, the Sun JVM uses uses '/' and not '\' (File.separatorChar) in the classpath
        // thanks to jeremy chone (http://www.bitsandbuzz.com/) for this insight.
        //
        // NOTE : for non-Sun JVMs, i have not idea what the behavior is.  but i'm not going to lose
        // any sleep over it. ;)
        packageName = packageName.replace('.', '/');
        Pattern packageDirMatcher = Pattern.compile("(" + Pattern.quote(packageName) + "(/.*)?)$");

        Enumeration<URL> dirs = classLoader.getResources(packageName);

        while (dirs.hasMoreElements()) {
            String path = URLDecoder.decode(dirs.nextElement().getPath(), "UTF-8");

            if (path.contains(".jar!") || path.contains("zip!")) {
                String jarName = path.substring("file:".length());
                jarName = jarName.substring(0, jarName.indexOf('!'));

                JarFile jarFile = new JarFile(jarName);
                if(classpathFilter.acceptJarFile(jarFile)) {
                    visitJarFile(jarFile, packageName, visitor);
                }
            }
            else {
                File dir = new File(path);
                Matcher dirMatcher = packageDirMatcher.matcher(path);
                
                if(dirMatcher.find() && classpathFilter.acceptDirectory(dir)) {
                    visitDirectory(dir, packageDirMatcher, visitor);
                }
            }
        }
    }


    protected void visitJarFile(JarFile jarFile, String packageNameForJarPath, ResourceVisitor<T> visitor) {

        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();

            String entryPackage = StringUtils.substringBeforeLast(entry.getName(), "/");
            if(packageNameForJarPath.equals(entryPackage) || (recursive && entryPackage.startsWith(packageNameForJarPath))) {

                String packageName = entryPackage.replace('/', '.');
                if(!entry.isDirectory() && resourceNameFilter.acceptResourceName(packageName, entry.getName())) {

                    T resource = resourceLoader.loadFromJarfile(packageName, jarFile, entry);
                    if(resource != null && resourceFilter.acceptScannedResource(resource)) {
                        visitor.visit(resource);
                    }
                }
            }
        }
    }


    protected void visitDirectory(File dir, Pattern packageDirMatcher, ResourceVisitor<T> visitor) {

        for(Object obj : FileUtils.listFiles(dir, null, recursive)) {
            File file = (File) obj;

            // because the JVM appears to use '/' on all platforms in the classpath entries, this pattern
            // always ends up with '/' and never File.separatorChar.  so, on windows, we'll need to modify
            // our search pattern.
            String absolutePath = file.getParentFile().getAbsolutePath();
            if(File.separatorChar != '/') {
                absolutePath = absolutePath.replace(File.separatorChar, '/');
            }

            Matcher dirMatcher = packageDirMatcher.matcher(absolutePath);
            if(dirMatcher.find()) {
                String packageNameForDir = dirMatcher.group(1).replace('/', '.');

                if(resourceNameFilter.acceptResourceName(packageNameForDir, file.getName())) {

                    T resource = resourceLoader.loadFromFilesystem(packageNameForDir, file.getParentFile(), file.getName());
                    if(resource != null && resourceFilter.acceptScannedResource(resource)) {
                        visitor.visit(resource);
                    }
                }
            }
        }
    }


    public static ClassLoader getDefaultClassLoader() {
        return PackageScanner.class.getClassLoader();
    }
}
