/*
 * Copyright 2012 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner;

import java.util.*;

/**
 * Groups a set of {@link ResourceFilter}s as a boolean AND, OR, or NOT condition.
 *
 * @see ResourceFilter
 * @see PackageScanner
 *
 * @Deprecated use {@link com.metapossum.utils.scanner.filters.ChainedResourceFilter}
 *
 * @author Steve van Loben Sels
 */
@Deprecated
public class ChainedResourceFilter<T> implements ResourceFilter<T> {

    public enum Mode {
        And, Or, Not
    }

    protected List<ResourceFilter<T>> filters;
    protected Mode mode;

    /**
     * creates an instance as {@link Mode#And}.
     * @param filters
     */
    public ChainedResourceFilter(ResourceFilter<T>... filters) {
        this(Mode.And, filters);
    }

    public ChainedResourceFilter(Mode mode, ResourceFilter<T>... filters) {

        if(filters == null || filters.length == 0) {
            throw new IllegalArgumentException("no filters provided");
        }

        this.filters = Arrays.asList(filters);
        this.mode = mode;
    }

    @Override
    public boolean acceptScannedResource(T item) {
        
        for(ResourceFilter<T> filter : filters) {
            boolean accepted = filter.acceptScannedResource(item);

            if(accepted) {
                if(mode == Mode.Or) {
                    return true;
                }

                if(mode == Mode.Not) {
                    return false;
                }
            }
            else if(mode == Mode.And) {
                return false;
            }
        }

        // And and Not are fail-fast, so if we reach the end, then our criteria is met.
        // Or would have already returned if it were successful.
        return mode == Mode.And || mode == Mode.Not;
    }
}
