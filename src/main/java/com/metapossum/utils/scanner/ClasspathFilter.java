/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner;

import java.io.*;
import java.util.jar.*;

/**
 * Filter for classpath entries.
 *
 * <p>Provides a mechanism to filter the top-level classpath entries.  For example, if
 * the classpath is <code>jar1.jar:jar2.jar:/home/user/classes:.</code> and each
 * entry contains the target package, the calls will be:
 * 
 * <ul>
 *   <li>{@link #acceptJarFile(java.util.jar.JarFile)} with jar1.jar</li>
 *   <li>{@link #acceptJarFile(java.util.jar.JarFile)} with jar2.jar</li>
 *   <li>{@link #acceptDirectory(java.io.File)} with <code>/home/user/classes</code></li>
 *   <li>{@link #acceptDirectory(java.io.File)} with <code>.</code></li>
 * </ul>
 *
 * </p>
 *
 * @see PackageScanner
 *
 * @author Steve van Loben Sels
 */
public interface ClasspathFilter {

    /**
     * Convenience implementation that doesn't filter anything.
     */
    public static class AcceptEverythingClasspathFilter implements ClasspathFilter {

        @Override
        public boolean acceptJarFile(JarFile jarFile) {
            return true;
        }

        @Override
        public boolean acceptDirectory(File directory) {
            return true;
        }
    }


    /**
     * return true if the calling {@link PackageScanner} should proceed with <code>jarFile</code>.
     */
    public boolean acceptJarFile(JarFile jarFile);

    /**
     * return true if the calling {@link PackageScanner} should proceed with <code>directory</code>.
     */
    public boolean acceptDirectory(File directory);
}
