/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner.loaders;

import com.metapossum.utils.scanner.*;

import java.io.*;
import java.net.*;
import java.util.jar.*;

/**
 * an implementation of {@link ResourceLoader} that returns resources as {@Link #URL urls}.
 *
 * @see com.metapossum.utils.scanner.ResourceLoader
 * @see com.metapossum.utils.scanner.PackageScanner
 *
 * @author Steve van Loben Sels
 * @since 1.0.1
 */
public class UrlResourceLoader implements ResourceLoader<URL> {

    @Override
    public URL loadFromJarfile(String packageName, JarFile jarFile, JarEntry entry) {
        return createIgnoringMalformedException("jar:file:" + jarFile.getName() + "!/" + entry.getName());
    }

    @Override
    public URL loadFromFilesystem(String packageName, File directory, String fileName) {
        return createIgnoringMalformedException(new File(directory, fileName).toURI().toString());
    }


    protected URL createIgnoringMalformedException(String urlStr) {
        try {
            return new URL(urlStr);
        }
        catch(MalformedURLException e) {
            // todo - logging?
            return null;
        }
    }
}
