/*
 * Copyright 2011 Steve van Loben Sels
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.metapossum.utils.scanner.filters;

import com.metapossum.utils.scanner.*;

import java.util.regex.*;

/**
 * a regular-expression based filter.
 *
 * the regular expression will be applied to the file name, and an optional
 * regex can also be configured to match against the package name.
 *
 * @see com.metapossum.utils.scanner.ResourceFilter
 * @see com.metapossum.utils.scanner.PackageScanner
 *
 * @author Steve van Loben Sels
 * @since 1.0.1
 */
public class RegexResourceNameFilter implements ResourceNameFilter {

    private Pattern pattern;
    private Pattern packagePattern;
    private boolean completeMatch;

    public RegexResourceNameFilter(String regex) {
        this(Pattern.compile(regex));
    }

    public RegexResourceNameFilter(Pattern pattern) {
        this.pattern = pattern;
    }

    /**
     * @return whether a complete match of the regex is required (this maps to {@link java.util.regex.Matcher#matches()}
     *         versus {@link java.util.regex.Matcher#find()}).
     */
    public boolean isCompleteMatch() {
        return completeMatch;
    }

    /**
     * set whether a complete match of the regex is required
     * @see #isCompleteMatch()
     */
    public RegexResourceNameFilter setCompleteMatch(boolean completeMatch) {
        this.completeMatch = completeMatch;
        return this;
    }

    /**
     * sets an optional regex that gets applied to the package.
     * @param regex a string regular expression to match against the package
     * @return <code>this</code> to allow for method chaining
     */
    public RegexResourceNameFilter setPackagePattern(String regex) {
        return setPackagePattern(Pattern.compile(regex));
    }

    /**
     * sets an optional regex that gets applied to the package.
     * @param packagePattern a pre-compiled regular expression to match against the package
     * @return <code>this</code> to allow for method chaining
     */
    public RegexResourceNameFilter setPackagePattern(Pattern packagePattern) {
        this.packagePattern = packagePattern;
        return this;
    }

    @Override
    public boolean acceptResourceName(String packageName, String fileName) {
        return (packagePattern == null || doMatch(packagePattern, packageName)) && doMatch(pattern, fileName);
    }


    protected boolean doMatch(Pattern pattern, String target) {
        Matcher matcher = pattern.matcher(target);
        return completeMatch ? matcher.matches() : matcher.find();
    }
}
